﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Timers;

namespace WebsitePingerConsole
{
    class WebsitePingerInfo
    {
        private static System.Timers.Timer timer;
        public static List<String> WebsiteList = new List<string>(){"http://snowgeek.org/", "http://esports.berkeley.edu/", "http://nanoclub.berkeley.edu/", "http://705bsp.org/"};
        public static void PingWebsiteList(object source, ElapsedEventArgs e)
        {
            foreach (String website in WebsiteList)
            {
                WebRequest request;
                request = WebRequest.Create(website);
                Stream response;
                response = request.GetResponse().GetResponseStream();
            }
        }
        public static void BeginIntervalRequests()
        {
            timer = new Timer();
            timer.Interval = 1000;
            timer.Elapsed += new ElapsedEventHandler(PingWebsiteList);
            timer.Start();
        }
    }
}
