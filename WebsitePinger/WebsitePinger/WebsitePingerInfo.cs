﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Timers;

namespace WebsitePinger
{
    class WebsitePingerInfo
    {
        public static List<String> WebsiteList = new List<string>(){"http://snowgeek.org/", "http://esports.berkeley.edu/", "http://nanoclub.berkeley.edu/", "http://705bsp.org/"};
        public static Timer timer;
        public static void PingWebsiteList(object state, ElapsedEventArgs elapsedEventArgs)
        {
            foreach (String website in WebsiteList)
            {
                WebRequest request;
                request = WebRequest.Create(website);
                request.Timeout = 10000;
                Stream response;
                response = request.GetResponse().GetResponseStream();
            }
            using (StreamWriter w = File.AppendText(@"c:\\WebPingerLog\log.txt"))
            {
                w.WriteLine("Pings completed at " + DateTime.Now + DateTime.Now.Second);
                w.Close();
            }
        }
        public static void BeginIntervalRequests()
        {
            timer = new Timer();
            timer.Elapsed += new ElapsedEventHandler(PingWebsiteList);
            timer.Interval = 5*60000;
            timer.Start();

        }
    }
}
